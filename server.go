package main

import (
	"bitbucket.org/MarcelBuian/fast-track/memory"
	"bitbucket.org/MarcelBuian/fast-track/routes"
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func runServer() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	questions, err := trivia.CreateQuestions("")
	if err != nil {
		log.Fatal(err)
	}
	users, err := memory.CreateUsers()
	if err != nil {
		log.Fatal(err)
	}

	router := mux.NewRouter()
	router.HandleFunc("/questions", routes.GetQuestions(*questions)).Methods("GET")
	router.HandleFunc("/questions/{id}/answer", routes.PostAnswer(*questions, users)).Methods("POST")
	router.HandleFunc("/questions/{id}/results", routes.GetResult(*questions, users)).Methods("GET")
	router.HandleFunc("/questions/results", routes.GetAllResult(*questions, users)).Methods("GET")
	handler := handlers.LoggingHandler(os.Stdout, router)

	srv := &http.Server{
		Addr: ":3001",
		Handler: handler,
	}

	log.Printf("running server on port %s\n", srv.Addr)
	log.Fatal(srv.ListenAndServe())
}

func main() {
	runServer()
}