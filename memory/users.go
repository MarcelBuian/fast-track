package memory

import "errors"

type Users []User

func CreateUsers() (*Users, error) {
	users := Users{}

	return &users, nil
}

func (users Users) Find(id int) *User {
	for _, u := range users {
		if u.ID == id {
			return &u
		}
	}
	return nil
}

func (users Users) Create(id int) *User {
	u := createNewUser()
	u.ID = id

	return u
}

func (users *Users) FindOrCreate(userId int) *User {
	user := users.Find(userId)
	if user != nil {
		return user
	}

	return users.Create(userId)
}

func (users *Users) AppendOrUpdate(updateUser *User) {
	newUsers := Users{}
	found := false
	for _, user := range *users {
		if user.ID == updateUser.ID {
			newUsers = append(newUsers, *updateUser)
			found = true
			continue
		}
		newUsers = append(newUsers, user)
	}

	if !found {
		newUsers = append(newUsers, *updateUser)
	}

	*users = newUsers
}

func (users *Users) BetterThanOthersBy(refUser *User, refQuestionId int) (float64, error) {
	totalUsersAnswers := 0
	refBetterAnswers := 0

	for _, refAnswer := range refUser.Answers {
		if refQuestionId != 0 && refAnswer.QuestionId != refQuestionId {
			continue
		}
		for _, user := range *users {
			if user.ID == refUser.ID {
				continue
			}
			for _, answer := range user.Answers {
				if answer.QuestionId != refAnswer.QuestionId {
					continue
				}
				totalUsersAnswers++
				if refAnswer.Correct && !answer.Correct {
					refBetterAnswers++
				}
			}
		}
	}

	if totalUsersAnswers == 0 {
		return float64(0), errors.New("no other answers")
	}

	val := float64(refBetterAnswers) / float64(totalUsersAnswers) * 100

	return val, nil
}