package memory

import (
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"errors"
)

type User struct {
	ID int
	Answers []Answer
}

func createNewUser() *User {
	u := User{}
	u.Answers = []Answer{}

	return &u
}

func (user User) GetAnswer(questionId int) (*Answer, error) {
	for _, answer := range user.Answers {
		if answer.QuestionId == questionId {
			return &answer, nil
		}
	}

	return nil, errors.New("answer not found")
}

func (user *User) StoreAnswer(question trivia.Question, response string) error {
	for _, answer := range user.Answers {
		if answer.QuestionId == question.ID {
			return errors.New("you already answered with: "+answer.Response)
		}
	}

	answer, err := createAnswer(question, response)
	if err != nil {
		return err
	}
	user.Answers = append(user.Answers, *answer)

	return nil
}