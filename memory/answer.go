package memory

import (
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"errors"
)

type Answer struct {
	QuestionId int
	Response string
	Correct bool
}

func createAnswer(question trivia.Question, response string) (*Answer, error) {
	if response == "" {
		return nil, errors.New("missing answer")
	}
	if !question.HasAnswer(response) {
		return nil, errors.New("answer "+response+" cannot be accepted")
	}

	a := Answer{}
	a.QuestionId = question.ID
	a.Response = response
	a.Correct = question.CorrectAnswer == response

	return &a, nil
}