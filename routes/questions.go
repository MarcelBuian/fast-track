package routes

import (
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

func errorResponse(w http.ResponseWriter, statusCode int, message string) {
	log.Println(message)
	w.WriteHeader(statusCode)
	_ : fmt.Fprint(w, message)
}

func GetQuestions(qs trivia.Questions) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var hiddenAnswers = qs.GetHiddenAnswers()
		w.Header().Add("content-type", "application/json")
		err := json.NewEncoder(w).Encode(hiddenAnswers)
		if err != nil {
			errorResponse(w, http.StatusInternalServerError, "Opps, an error happened; try again!")
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}