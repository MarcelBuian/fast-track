package routes

import (
	"bitbucket.org/MarcelBuian/fast-track/memory"
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

func PostAnswer(questions trivia.Questions, users *memory.Users) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userIdString := r.Header.Get("userId")
		if userIdString == "" {
			errorResponse(w, http.StatusBadRequest, "userId header must be provided in order to post an answer")
		}
		userId, err := strconv.Atoi(userIdString)
		if err != nil {
			errorResponse(w, http.StatusBadRequest, "unable to convert userId to integer")
			return
		}
		questionIdString, ok := mux.Vars(r)["id"]
		if !ok {
			errorResponse(w, http.StatusNotFound, "can't find question id in route")
			return
		}
		questionId, err := strconv.Atoi(questionIdString)
		if err != nil {
			errorResponse(w, http.StatusBadRequest, "unable to convert questionId to integer")
			return
		}
		question, err := questions.Find(questionId)
		if err != nil {
			errorResponse(w, http.StatusNotFound, "Invalid question")
			return
		}
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			errorResponse(w, http.StatusUnprocessableEntity, "unable to decode response")
			return
		}
		response := string(body)

		user := users.FindOrCreate(userId)
		err = user.StoreAnswer(*question, response)
		if err != nil {
			errorResponse(w, http.StatusUnprocessableEntity, err.Error())
			return
		}
		users.AppendOrUpdate(user)
		log.Print(users)

		w.WriteHeader(http.StatusCreated)
		_ : fmt.Fprint(w, "The answer " + response +" is accepted")
	}
}