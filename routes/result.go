package routes

import (
	"bitbucket.org/MarcelBuian/fast-track/memory"
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
)

func GetResult(questions trivia.Questions, users *memory.Users) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userIdString := r.Header.Get("userId")
		if userIdString == "" {
			errorResponse(w, http.StatusBadRequest, "userId header must be provided in order to post an answer")
		}
		userId, err := strconv.Atoi(userIdString)
		if err != nil {
			errorResponse(w, http.StatusBadRequest, "unable to convert userId to integer")
			return
		}
		questionIdString, ok := mux.Vars(r)["id"]
		if !ok {
			errorResponse(w, http.StatusNotFound, "can't find question id in route")
			return
		}
		questionId, err := strconv.Atoi(questionIdString)
		if err != nil {
			errorResponse(w, http.StatusBadRequest, "unable to convert questionId to integer")
			return
		}
		_, err = questions.Find(questionId)
		if err != nil {
			errorResponse(w, http.StatusNotFound, "Invalid question")
			return
		}

		user := users.FindOrCreate(userId)
		answer, err := user.GetAnswer(questionId)

		log.Println(users)
		if err != nil {
			_ : fmt.Fprint(w, "The answer doesn't exists")
			return
		}

		response := "Your response is: "+answer.Response+"."

		if answer.Correct {
			response +=" Your answer is correct."
		} else {
			response +=" Your answer is wrong."
		}

		betterPercentage, err := users.BetterThanOthersBy(user, questionId)
		if err != nil {
			response +=" There are no other answers to compare with."
			_ : fmt.Fprint(w, response)
			return
		}

		val := strconv.FormatFloat(betterPercentage, 'f', 2, 64)
		response +=" You were better than "+val+"% of all quize."

		_ : fmt.Fprint(w, response)
	}
}

func GetAllResult(questions trivia.Questions, users *memory.Users) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		userIdString := r.Header.Get("userId")
		if userIdString == "" {
			errorResponse(w, http.StatusBadRequest, "userId header must be provided in order to post an answer")
		}
		userId, err := strconv.Atoi(userIdString)
		if err != nil {
			errorResponse(w, http.StatusBadRequest, "unable to convert userId to integer")
			return
		}

		user := users.FindOrCreate(userId)

		betterPercentage, err := users.BetterThanOthersBy(user, 0)
		if err != nil {
			_ : fmt.Fprint(w, "There are no other answers to compare with.")
			return
		}

		val := strconv.FormatFloat(betterPercentage, 'f', 2, 64)

		_ : fmt.Fprint(w, "You were better than "+val+"% of all quize.")
	}
}