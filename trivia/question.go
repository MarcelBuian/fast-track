package trivia

import (
	"math/rand"
	"time"
)

type Question struct {
	ID               int      `json:"id,omitempty"`
	Question         string   `json:"question,omitempty"`
	CorrectAnswer    string   `json:"correct_answer,omitempty"`
	IncorrectAnswers []string `json:"incorrect_answers,omitempty"`
	PossibleAnswers  []string `json:"possible_answer,omitempty"`
}

func (q *Question) generatePossibleAnswers() {
	var answerList []string
	answerList = append(answerList, q.IncorrectAnswers...)
	answerList = append(answerList, q.CorrectAnswer)
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(answerList), func(i int, j int) {
		answerList[i], answerList[j] = answerList[j], answerList[i]
	})
	q.PossibleAnswers = answerList
}

func (q *Question) hideCorrectIncorrectAnswers() {
	q.CorrectAnswer = ""
	q.IncorrectAnswers = []string{}
}

func (q *Question) HasAnswer(answer string) bool {
	if answer == q.CorrectAnswer {
		return true
	}
	for _, a := range q.IncorrectAnswers {
		if a == answer {
			return true
		}
	}
	for _, a := range q.PossibleAnswers {
		if a == answer {
			return true
		}
	}

	return false
}