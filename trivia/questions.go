package trivia

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

type Questions []Question

func CreateQuestions(path string) (*Questions, error) {
	if path == "" {
		path = "store/questions.json"
	}
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	questions := Questions{}
	err = json.Unmarshal(file, &questions)
	if err != nil {
		return nil, err
	}

	return &questions, nil
}

func (questions Questions) Find(id int) (*Question, error) {
	for _, q := range questions {
		if q.ID == id {
			return &q, nil
		}
	}

	return nil, errors.New("question not found")
}

func (questions Questions) GetHiddenAnswers() *Questions {
	newQuestions := Questions{}
	for _, q := range questions {
		q.generatePossibleAnswers()
		q.hideCorrectIncorrectAnswers()
		newQuestions = append(newQuestions, q)
	}

	return &newQuestions
}
