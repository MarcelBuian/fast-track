
* Run application:

1. Run server: `go run server.go`
2. Run client: `go run client/main.go menu`

* Run tests: `go test`