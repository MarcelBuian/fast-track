package main

import (
	"bitbucket.org/MarcelBuian/fast-track/memory"
	"bitbucket.org/MarcelBuian/fast-track/routes"
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"bytes"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"
)

func fail(t *testing.T, message string) {
	t.Error(message)
	t.Fail()
}

func getQuestions(t *testing.T) *trivia.Questions {
	questions, err := trivia.CreateQuestions("")
	if err != nil {
		fail(t, err.Error())
	}

	return questions
}

func getUsers(t *testing.T) *memory.Users {
	users, err := memory.CreateUsers()
	if err != nil {
		fail(t, err.Error())
	}

	return users
}
func TestGetQuestions(t *testing.T) {
	questions := getQuestions(t)

	url := "/questions"

	req, _ := http.NewRequest("GET", url, nil)
	response := httptest.NewRecorder()
	router := mux.NewRouter()
	router.HandleFunc(url, routes.GetQuestions(*questions))
	router.ServeHTTP(response, req)
	if response.Code != 200 {
		fail(t, string(response.Code))
	}
	responseList := trivia.Questions{}
	if err := json.NewDecoder(response.Body).Decode(&responseList); err != nil {
		fail(t, err.Error())
	}
	memoryQuestion, err := questions.Find(1)
	if err != nil {
		fail(t, err.Error())
	}
	responseQuestion, err := responseList.Find(1)
	if err != nil {
		fail(t, err.Error())
	}

	if responseQuestion.Question != memoryQuestion.Question {
		fail(t, "Expected question is different than given one")
	}
	if responseQuestion.CorrectAnswer != "" {
		fail(t, "Correct Answer must be empty")
	}
	if len(responseList) != len(*questions) {
		fail(t, "Expected questions length are different than given")
	}
}

func TestPostAnswers(t *testing.T) {
	questions := getQuestions(t)
	users := getUsers(t)
	question := (*questions)[0]
	userId := 777
	url := "/questions/"+strconv.Itoa(question.ID)+"/answer"
	rawUrl := "/questions/{id}/answer"
	correctAnswer := question.CorrectAnswer
	postAnswer := correctAnswer

	req, _ := http.NewRequest("POST", url, bytes.NewReader([]byte(postAnswer)))

	req.Header.Set("userId", strconv.Itoa(userId))
	response := httptest.NewRecorder()

	router := mux.NewRouter()
	router.HandleFunc(rawUrl, routes.PostAnswer(*questions, users))
	router.ServeHTTP(response, req)
	if response.Code != 201 {
		fail(t, strconv.Itoa(response.Code))
	}

	resultAnswer := string(response.Body.Bytes())
	if resultAnswer != "The answer "+ postAnswer +" is accepted" {
		fail(t, "unexpected answer: "+resultAnswer)
	}
}

func TestGetResult(t *testing.T) {
	questions := getQuestions(t)
	users := getUsers(t)
	question := (*questions)[0]
	userId := 777
	url := "/questions/"+strconv.Itoa(question.ID)+"/result"
	rawUrl := "/questions/{id}/result"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Set("userId", strconv.Itoa(userId))
	response := httptest.NewRecorder()

	router := mux.NewRouter()
	router.HandleFunc(rawUrl, routes.GetResult(*questions, users))
	router.ServeHTTP(response, req)
	if response.Code != 200 {
		fail(t, strconv.Itoa(response.Code))
	}

	resultAnswer := string(response.Body.Bytes())
	if resultAnswer != "The answer doesn't exists" {
		fail(t, "unexpected answer: "+resultAnswer)
	}
}

func TestGetAllResult(t *testing.T) {
	questions := getQuestions(t)
	users := getUsers(t)
	url := "/questions/results"
	*users = append(*users, memory.User{ID: 13, Answers: []memory.Answer{
		{QuestionId: 1, Correct:false},
	}})
	*users = append(*users, memory.User{ID: 14, Answers: []memory.Answer{
		{QuestionId: 1, Correct:true},
	}})
	*users = append(*users, memory.User{ID: 15, Answers: []memory.Answer{
		{QuestionId: 1, Correct:false},
	}})
	*users = append(*users, memory.User{ID: 16, Answers: []memory.Answer{
		{QuestionId: 1, Correct:true},
	}})
	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Set("userId", strconv.Itoa(16))
	response := httptest.NewRecorder()

	router := mux.NewRouter()
	router.HandleFunc(url, routes.GetAllResult(*questions, users))
	router.ServeHTTP(response, req)
	if response.Code != 200 {
		fail(t, strconv.Itoa(response.Code))
	}

	resultAnswer := string(response.Body.Bytes())
	if resultAnswer != "You were better than 66.67% of all quize." {
		fail(t, "unexpected answer: "+resultAnswer)
	}
}
