package main

import (
	"bitbucket.org/MarcelBuian/fast-track/client/cmd"
)

func main() {
	cmd.Execute()
}