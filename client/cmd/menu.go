package cmd

import (
	"bitbucket.org/MarcelBuian/fast-track/trivia"
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

const BASE_URL = "http://localhost:3001"

func containsInt(need int, arr []int) bool {
	for _, el := range arr {
		if el == need {
			return true
		}
	}
	return false
}

func readInt(actionText string, validate func(number int) bool) int {
	scanner := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print(actionText)
		scanner.Scan()
		line := scanner.Text()
		number, err := strconv.Atoi(line)
		if err != nil {
			fmt.Println("Input must be numeric. Please retry.")
			continue
		}
		if validate(number) {
			return number
		}
		fmt.Println("Invalid input. Please retry.")
	}
}

func readQuestions() *trivia.Questions {
	url := BASE_URL+"/questions"

	req, _ := http.NewRequest("GET", url, nil)
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Request cannot be done. Please start server and retry.")
		fmt.Println(err)
		return &trivia.Questions{}
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	questions := trivia.Questions{}
	_ = json.Unmarshal(body, &questions)

	return &questions
}

func postAnswer(userId int, questionId int, answer string) string {
	url := BASE_URL+"/questions/"+strconv.Itoa(questionId)+"/answer"

	req, _ := http.NewRequest("POST", url, bytes.NewReader([]byte(answer)))
	req.Header.Add("userId", strconv.Itoa(userId))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Request cannot be done. Please start server and retry.")
		fmt.Println(err)
		return "Error"
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	return string(body)
}

func getResults(userId int) string {
	url := BASE_URL+"/questions/results"

	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Add("userId", strconv.Itoa(userId))
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Request cannot be done. Please start server and retry.")
		fmt.Println(err)
		return "Error"
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	return string(body)
}

var menuCmd = &cobra.Command{
	Use: "menu",
	Run: func(cmd *cobra.Command, args []string) {
		userId := 1
		MenuLoop: for {
			fmt.Println("\nWelcome, userId = " + strconv.Itoa(userId))
			fmt.Println("1 = Change userId")
			fmt.Println("2 = Answer questions")
			fmt.Println("3 = See results")
			fmt.Println("9 = Exit")
			action := readInt("Choose a numeric action: ", func(n int) bool {
				return containsInt(n, []int{1,2,3,9})
			})
			switch action {
			case 1: userId = readInt("New userId: ", func(n int) bool {
				return n!= userId
			})
			case 2:
				questions := *readQuestions()
				qSize := len(questions)
				QuestionsLoop: for qi, q := range questions {
					answers := q.PossibleAnswers
					fmt.Println("\nQuestion "+strconv.Itoa(qi+1)+" / "+strconv.Itoa(qSize))
					fmt.Println(q.Question)
					var indexAnswers [100]string
					fmt.Println("-1 = Back to menu")
					fmt.Println("0 = Skip the answer")
					for i, a := range answers {
						indexAnswers[i+1] = a
						fmt.Println(strconv.Itoa(i+1)+" = "+a)
					}
					responseIndex := readInt("Choose a numeric answer: ", func(n int) bool {
						return n >= -1 && n <= len(answers)
					})
					if responseIndex == -1 {
						fmt.Println("OK. No more questions for now")
						break QuestionsLoop
					}
					if responseIndex > 0 {
						answer := indexAnswers[responseIndex]
						response := postAnswer(userId, q.ID, answer)
						fmt.Println(response)
					}
				}
			case 3: fmt.Println(getResults(userId))
			case 9: fmt.Println("Have a lovely day!"); break MenuLoop
			default: fmt.Println("Todo")
			}
		}
		fmt.Println("Bye!")
	},
}

func init() {
	RootCmd.AddCommand(menuCmd)
}
